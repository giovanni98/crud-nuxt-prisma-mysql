import { ComputedRef, Ref } from 'vue'
export type LayoutKey = string
declare module "/home/robot/Documents/Nuxt/GIT-deposit/crud-nuxt-prisma-mysql/node_modules/nuxt/dist/pages/runtime/composables" {
  interface PageMeta {
    layout?: false | LayoutKey | Ref<LayoutKey> | ComputedRef<LayoutKey>
  }
}