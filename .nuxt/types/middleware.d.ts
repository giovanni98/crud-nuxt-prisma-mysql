import type { NavigationGuard } from 'vue-router'
export type MiddlewareKey = string
declare module "/home/robot/Documents/Nuxt/GIT-deposit/crud-nuxt-prisma-mysql/node_modules/nuxt/dist/pages/runtime/composables" {
  interface PageMeta {
    middleware?: MiddlewareKey | NavigationGuard | Array<MiddlewareKey | NavigationGuard>
  }
}